package com.codetest.parenthesis;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class MatchingParenthesis {

    private static final Map<Character, Character> matchingBrackets = new HashMap<Character, Character>() {{
        put('[', ']');
        put('(', ')');
        put('{', '}');
    }};

    public boolean isMatchingParenthesis(String input) {
        Stack<Character> parenthesisStack = new Stack<>();
        if (input == null || "".equalsIgnoreCase(input)) return false;

        Set<Character> allOpenParenthesis = matchingBrackets.keySet();
        for (int i = 0; i < input.length(); i++) {
           Character currentParenthesis = input.charAt(i);
           if (allOpenParenthesis.contains(currentParenthesis)) {
               parenthesisStack.push(currentParenthesis);
           } else if (!parenthesisStack.empty()){
               Character previousParenthesis = parenthesisStack.pop();
               if (!matchingBrackets.get(previousParenthesis).equals(currentParenthesis)) {
                   return false;
               }
           } else {
               return false;
           }
        }
        return parenthesisStack.isEmpty();
    }

}
