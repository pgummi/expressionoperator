package com.codetest.expressionoperator;

public class ExpressionOperator {

    /**
     * Perform operation based on input string 5 + 6 -7*8
     */
    public static int performOp(String input) {
        //validate null input
        if (input == null || input.length() == 0) {
            return 0;
        }
        char[] inputChars = input.toCharArray();
        //perform operations based on char array
        return performOp(inputChars, 0, inputChars.length - 1);
    }

    private static int performOp(char[] input, int start, int end) {
        String nextNumber = null;
        //validate empty char array
        if (input == null || input.length == 0 || (start == end)) {
            return 0;
        }

        //iterate through each character
        for (int i = start; i < input.length; i++) {
            //ignore, if space character
            if (Character.isSpaceChar(input[i])) continue;
            //if digit create a new number or append to previous digit character
            if (Character.isDigit(input[i])) {
                if (nextNumber == null) {
                    nextNumber = new String(new char[]{input[i]});
                } else {
                    nextNumber += input[i];
                }
            } else if (input[i] == '+' ) {
                // perform add operation if + sign and continue operation on next characters
               return Integer.parseInt(nextNumber) + performOp(input, i+1, input.length-1);
            } else if (input[i] == '-') {
                // perform subtract operation if - sign and continue operation on next characters
                return Integer.parseInt(nextNumber) - performOp(input, i+1, input.length-1);
            } else if (input[i] == '*') {
                // perform multiply operation if * sign and continue operaton on next characters
                return Integer.parseInt(nextNumber) * performOp(input, i+1, input.length-1);
            } else if ( input[i] == '/') {
                // perform multiply operation if / sign and continue operation on next characters
                return Integer.parseInt(nextNumber) / performOp(input, i+1, input.length-1);
            }
        }
        //if no operation sign, it is the last number
        return Integer.parseInt(nextNumber);
    }

}
