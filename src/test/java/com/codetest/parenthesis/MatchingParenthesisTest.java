package com.codetest.parenthesis;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class MatchingParenthesisTest {

   MatchingParenthesis matchingBrackets = new MatchingParenthesis();

   @Test
   public void testMatchingBrackets() {

      String nullParenthesis = null;
      boolean nullParenthesisResult = matchingBrackets.isMatchingParenthesis(nullParenthesis);
      assertTrue(nullParenthesis + " has matching parenthesis: ", !nullParenthesisResult);

      String emptyParenthesis = "";
      boolean s0Boolean = matchingBrackets.isMatchingParenthesis(emptyParenthesis);
      assertTrue(emptyParenthesis + " has matching parenthesis: ", !s0Boolean);

      String singleParenthesis = "[]";
      boolean s1Boolean = matchingBrackets.isMatchingParenthesis(singleParenthesis);
      assertTrue(singleParenthesis + " has matching parenthesis: ", s1Boolean);

      String closedSquareBracket = "]";
      boolean closedSquareBracketResult = matchingBrackets.isMatchingParenthesis(closedSquareBracket);
      assertFalse(closedSquareBracket + " has matching parenthesis: ", closedSquareBracketResult);

      String closedSquareBracket2 = "][]";
      boolean closedSquareBracket2Result = matchingBrackets.isMatchingParenthesis(closedSquareBracket2);
      assertFalse(closedSquareBracket2 + " has matching parenthesis: ", closedSquareBracket2Result);

      String closedSquareBracket3 = "][";
      boolean closedSquareBracket3Result = matchingBrackets.isMatchingParenthesis(closedSquareBracket3);
      assertFalse(closedSquareBracket3 + " has matching parenthesis: ", closedSquareBracket3Result);

      String multipleParenthesis1 = "({{{}}})";
      boolean multipleParenthesisResult1 = matchingBrackets.isMatchingParenthesis(multipleParenthesis1);
      assertTrue(multipleParenthesis1 + " has matching parenthesis: ", multipleParenthesisResult1);

      String multipleParenthesis2 = "([][{{}}])[][]{}";
      boolean multipleParenthesisResult2 = matchingBrackets.isMatchingParenthesis(multipleParenthesis2);
      assertTrue(multipleParenthesis2 + " has matching parenthesis: ", multipleParenthesisResult2);

      String multipleParenthesis3 = "({}{}[])";
      boolean multipleParenthesisResult3 = matchingBrackets.isMatchingParenthesis(multipleParenthesis3);
      assertTrue(multipleParenthesis3 + " has matching parenthesis: ", multipleParenthesisResult3);

      String multipleParenthesis4 = "({}{[])}";
      boolean multipleParenthesisResult4 = matchingBrackets.isMatchingParenthesis(multipleParenthesis4);
      assertFalse(multipleParenthesis4 + " has matching parenthesis: ", multipleParenthesisResult4);

   }
}
