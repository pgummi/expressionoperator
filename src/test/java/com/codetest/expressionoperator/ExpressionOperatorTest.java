package com.codetest.expressionoperator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExpressionOperatorTest {

    ExpressionOperator expressionOperator = new ExpressionOperator();

    @Test
    public void testExpressions() {
        int result1 = expressionOperator.performOp(null);
        assertEquals("null = 0", result1, 0);

        int result2 = expressionOperator.performOp(" ");
        assertEquals(" = 0", result2, 0);

        int result3 = expressionOperator.performOp("5 + 4");
        assertEquals("5 + 4 = 9", result3, 9);

        int result4 = expressionOperator.performOp("5 / 4");
        assertEquals("5 / 4 = 1", result4, 1);

        String input5 = "5 + 6 -4 * 7 / 4";
        int result5 = expressionOperator.performOp(input5);
        assertEquals(input5 + " = 7", result5, 7);

        String input6 = "5 + 6 - 8 * 9 ";
        int result6 = expressionOperator.performOp(input6);
        assertEquals(input6 + " = -61", result6, -61);
    }
}
